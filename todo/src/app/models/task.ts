export class Task {

	public id: string;
	public description: string;
	public is_complete: boolean;
	constructor(){
		this.is_complete = false;
	}
}
