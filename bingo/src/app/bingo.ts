import { Number } from './number';

export class Bingo {
	numbers: Number[];
	center = 'UTN';
	is_winner: false;
	max_number = 75;
	bingo_size = 24;

	constructor() {
		this.numbers = [];
		while(this.numbers.length < this.bingo_size){
			let r = Math.floor(Math.random()*this.max_number) + 1;
			let random_number = new Number(String(r));
		    if (this.numbers.find(e => e === random_number)) {
		    	continue;
		    }
		    this.numbers.push(random_number);
		}
		this.numbers.splice(12, 0, new Number(this.center));
	}
}
